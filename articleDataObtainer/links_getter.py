import urllib3
import re
import requests
from bs4 import BeautifulSoup

def remove_duplicates(values):
    output = []
    seen = set()
    for value in values:
        if value not in seen:
            output.append(value)
            seen.add(value)
    return output

def prepare_list(links):
    list = []
    for element in links:
        list.append(element.replace('/blog/', ''))  
    return list

def get_tag_list():
    tagLinks = []
    tagList = []
    mainResp = requests.get('https://teonite.com/blog/')
    mainSoup = BeautifulSoup(mainResp.text, 'lxml')
    for link in mainSoup.find_all('a'):
        if '/blog/tag' in str(link.get('href')) and '/page/2' not in str(link.get('href')):
            tagLinks.append(str(link.get('href')))
    tagList = remove_duplicates(prepare_list(tagLinks))
    return tagList

def get_article_list():
    articleLinks = []
    articleList = []
    tagList = get_tag_list()
    for tag in tagList:
        tagResp = requests.get('https://teonite.com/blog/' + tag)
        tagSoup = BeautifulSoup(tagResp.text, 'lxml')
        for header in tagSoup.find_all('h2'):
            link = header.find('a')
            articleLinks.append(str(link.get('href')))
    articleList = remove_duplicates(prepare_list(articleLinks))
    return articleList            