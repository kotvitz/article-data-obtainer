import re
import requests
import psycopg2
import json
from bs4 import BeautifulSoup, NavigableString
from articleDataObtainer import db_config
from collections import Counter

def get_top_10_words():
    query = "SELECT article_content FROM public.articledata"
    conn = None
    try:
        params = db_config.config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query)
        contents = str(cur.fetchall())
        words = re.sub(r'[^\w\s]', "", contents)
        cnt = Counter(words.split())
        top_10 = cnt.most_common(10)
        cur.close()
        return top_10
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def get_top_10_words_by_author(author):
    query = "SELECT article_content FROM public.articledata WHERE author_shortname = '{0}'".format(author)
    conn = None
    try:
        params = db_config.config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query)
        contents = str(cur.fetchall())
        words = re.sub(r'[^\w\s]', "", contents)
        cnt = Counter(words.split())
        top_10 = cnt.most_common(10)
        cur.close()
        return top_10
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def get_authors():
    query = "SELECT author, author_shortname FROM public.articledata"
    conn = None
    try:
        params = db_config.config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query)
        authors = cur.fetchall()
        cur.close()
        return authors
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()                    