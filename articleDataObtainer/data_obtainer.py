import re
import requests
import psycopg2
import json
from bs4 import BeautifulSoup, NavigableString
from collections import Counter
from articleDataObtainer import db_config
from articleDataObtainer import links_getter

def insert_data_to_database():
    conn = None
    empty_base = check_if_database_is_empty()
    if empty_base is False:
        return
    links = links_getter.get_article_list()    
    for link in links:
        resp = requests.get('https://teonite.com/blog/' + link)
        soup = BeautifulSoup(resp.text, 'lxml')
        html_article = soup.find('article')
        html_author = html_article.find('span', {'class', 'author-content'}).find('h4')
        article_content = str(html_article.text)
        author = str(html_author.text)
        author_shortname = author.lower().replace(' ', '')
        query = "INSERT INTO public.articledata (article_content, author, author_shortname) VALUES (%s, %s, %s)"
        try:
            params = db_config.config()
            conn = psycopg2.connect(**params)
            cur = conn.cursor()
            cur.execute(query, (article_content, author, author_shortname))
            conn.commit()
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()          

def check_if_database_is_empty():
    conn = None
    query = 'SELECT * FROM public.articledata'
    try:
        params = db_config.config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query)
        data = cur.fetchall()
        cur.close()
        if data:
            return False
        else:
            return True
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()          