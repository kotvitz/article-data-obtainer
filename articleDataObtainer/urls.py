from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from articleDataObtainer import stats_builder
from articleDataObtainer import data_obtainer
import re
import requests
import json
import psycopg2

data_obtainer.insert_data_to_database()

top_10 = stats_builder.get_top_10_words()

authors = stats_builder.get_authors()

class StatsView(APIView):
    renderer_classes = (JSONRenderer, )
    def get(self, request, format=None):
        return Response(dict(top_10), content_type='application/json')

class AuthorStatsView(APIView):
    renderer_classes = (JSONRenderer, )
    def get_queryset(self):
        author = self.kwargs['author']
        top_10_by_author = stats_builder.get_top_10_words_by_author(str(author))
        return Response(dict(top_10_by_author), content_type='application/json')

    def get(self, request, **kwargs):
        return self.get_queryset() 

class AuthorsView(APIView):
    renderer_classes = (JSONRenderer, )
    def get(self, request, format=None):
        return Response(dict(authors), content_type='application/json')
    

urlpatterns = [
    path('', admin.site.urls),
    url(r'^stats/$', StatsView.as_view(), name='stats'),
    url(r'^stats/(?P<author>.+)/$', AuthorStatsView.as_view()),
    url(r'^authors/$', AuthorsView.as_view(), name='authors')
]    
